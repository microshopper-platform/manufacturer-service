CREATE TABLE manufacturer (
    id serial NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(2048),
    PRIMARY KEY (id)
);

CREATE TABLE manufacturer_account (
    id serial NOT NULL,
    manufacturer_id integer,
    username character varying(32) NOT NULL,
    password character varying(1024) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE manufacturer_account ADD CONSTRAINT FK_MANUFACTURER_ACCOUNT_MANUFACTURER FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id);

