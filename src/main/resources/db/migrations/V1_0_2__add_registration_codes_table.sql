CREATE TABLE registration_code (
    id serial NOT NULL,
    code character varying(64) NOT NULL,
    manufacturer_id integer,
    used boolean,
    role character varying(64) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE registration_code ADD CONSTRAINT FK_REGISTRATION_CODE_MANUFACTURER FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id);
