package com.microshopper.manufacturerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ManufacturerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManufacturerServiceApplication.class, args);
	}

}
