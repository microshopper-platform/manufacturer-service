package com.microshopper.manufacturerservice.configuration;

import com.microshopper.authservice.web.api.AuthApi;
import com.microshopper.authservice.web.restclient.ApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AuthApiConfiguration {

  private final RestTemplate restTemplate;

  @Autowired
  public AuthApiConfiguration(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Bean
  public ApiClient authApiClient() {
    ApiClient apiClient = new ApiClient(restTemplate);
    apiClient.setBasePath("http://auth-service");
    return apiClient;
  }

  @Bean
  public AuthApi authApi() {
    return new AuthApi(authApiClient());
  }
}
