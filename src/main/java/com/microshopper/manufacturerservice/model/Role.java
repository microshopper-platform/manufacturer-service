package com.microshopper.manufacturerservice.model;

public enum Role {

  ROLE_MANUFACTURER_ADMIN,

  ROLE_MANUFACTURER_TRANSPORTER;
}
