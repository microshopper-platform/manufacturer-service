package com.microshopper.manufacturerservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name = "manufacturer_account")
public class ManufacturerAccount {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "manufacturer_id")
  private Manufacturer manufacturer;

  @Column(name = "username")
  private String username;

  @Column(name = "email")
  private String email;

  @Column(name = "role")
  @Enumerated(value = EnumType.STRING)
  private Role role;
}
