package com.microshopper.manufacturerservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name = "registration_code")
public class RegistrationCode {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "code")
  private String code;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "manufacturer_id")
  private Manufacturer manufacturer;

  @Column(name = "used")
  private Boolean used = Boolean.FALSE;

  @Column(name = "role")
  @Enumerated(value = EnumType.STRING)
  private Role role;
}
