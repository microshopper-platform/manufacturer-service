package com.microshopper.manufacturerservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "ManufacturerAccount")
public class ManufacturerAccountDto {

  private Long id;

  private String username;

  private String email;

  private ManufacturerDto manufacturer;
}
