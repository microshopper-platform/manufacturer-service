package com.microshopper.manufacturerservice.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "ManufacturerRegistration")
public class ManufacturerRegistrationDto {

  @JsonProperty(required = true)
  private ManufacturerAccountRegistrationDto manufacturerAccountRegistration;

  @JsonIgnoreProperties(value = "id")
  @JsonProperty(required = true)
  private ManufacturerDto manufacturer;
}
