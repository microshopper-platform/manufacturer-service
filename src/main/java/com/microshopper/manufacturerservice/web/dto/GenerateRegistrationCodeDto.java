package com.microshopper.manufacturerservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "GenerateRegistrationCode")
public class GenerateRegistrationCodeDto {

  private Long manufacturerId;

  private RoleDto role;
}
