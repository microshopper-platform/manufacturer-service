package com.microshopper.manufacturerservice.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "ManufacturerAccountRegistration")
public class ManufacturerAccountRegistrationDto {

  @JsonProperty(required = true)
  private String username;

  @JsonProperty(required = true)
  private String password;

  @JsonProperty(required = true)
  private String email;

  @JsonProperty(required = true)
  private String registrationCode;
}
