package com.microshopper.manufacturerservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "Role")
public enum RoleDto {

  ROLE_MANUFACTURER_ADMIN,

  ROLE_MANUFACTURER_TRANSPORTER;
}
