package com.microshopper.manufacturerservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "Manufacturer")
public class ManufacturerDto {

  private Long id;

  private String name;

  private String description;
}
