package com.microshopper.manufacturerservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "RegistrationCode")
public class RegistrationCodeDto {

  private String code;

  private Boolean used;

  private RoleDto role;
}
