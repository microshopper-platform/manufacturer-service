package com.microshopper.manufacturerservice.web.controller;

import com.microshopper.manufacturerservice.service.ManufacturerAccountService;
import com.microshopper.manufacturerservice.service.ManufacturerService;
import com.microshopper.manufacturerservice.web.dto.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "manufacturer", description = "The Manufacturer API")
public class ManufacturerController {

  private static final String BASE_URL = "/api/manufacturers";

  private final ManufacturerService manufacturerService;
  private final ManufacturerAccountService manufacturerAccountService;

  @Autowired
  public ManufacturerController(
    ManufacturerService manufacturerService,
    ManufacturerAccountService manufacturerAccountService) {

    this.manufacturerService = manufacturerService;
    this.manufacturerAccountService = manufacturerAccountService;
  }

  @GetMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.OK)
  public List<ManufacturerDto> getAllManufacturers() {

    return manufacturerService.getAllManufacturers();
  }

  @GetMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public ManufacturerDto getManufacturer(@PathVariable Long id) {

    return manufacturerService.getManufacturerById(id);
  }

  @GetMapping(value = BASE_URL + "/accounts/{username}")
  @ResponseStatus(value = HttpStatus.OK)
  public ManufacturerAccountDto getManufacturerAccountByUsername(@PathVariable String username) {

    return manufacturerAccountService.getManufacturerAccountByUsername(username);
  }

  @PostMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.CREATED)
  public void registerManufacturer(@RequestBody ManufacturerRegistrationDto manufacturerRegistrationDto) {

    manufacturerAccountService.registerManufacturer(manufacturerRegistrationDto);
  }

  @PutMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.OK)
  public void updateManufacturerInformation(@RequestBody ManufacturerDto manufacturerDto) {

    manufacturerService.updateManufacturer(manufacturerDto);
  }

  @PostMapping(value = BASE_URL + "/accounts")
  @ResponseStatus(value = HttpStatus.CREATED)
  public void registerManufacturerAccount(
    @RequestBody ManufacturerAccountRegistrationDto manufacturerAccountRegistrationDto) {

    manufacturerAccountService.registerManufacturerAccount(manufacturerAccountRegistrationDto);
  }

  @PutMapping(value = BASE_URL + "/accounts")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateManufacturerAccount(@RequestBody ManufacturerAccountDto manufacturerAccountDto) {

    manufacturerAccountService.updateManufacturerAccount(manufacturerAccountDto);
  }

  @PostMapping(value = BASE_URL + "/registration-codes")
  @ResponseStatus(value = HttpStatus.CREATED)
  public RegistrationCodeDto generateNewRegistrationCode(
    @RequestBody GenerateRegistrationCodeDto generateRegistrationCodeDto) {

    return manufacturerAccountService.generateNewRegistrationCode(generateRegistrationCodeDto);
  }

  @GetMapping(value = BASE_URL + "/registration-codes/{manufacturerId}")
  @ResponseStatus(value = HttpStatus.OK)
  public List<RegistrationCodeDto> getAllRegistrationCodes(@PathVariable Long manufacturerId) {

    return manufacturerAccountService.getAllRegistrationCodes(manufacturerId);
  }

  @GetMapping(value = BASE_URL + "/{id}/transporters")
  @ResponseStatus(value = HttpStatus.OK)
  public List<ManufacturerAccountDto> getAllManufacturerTransporters(@PathVariable Long id) {

    return manufacturerAccountService.getAllManufacturerTransporters(id);
  }
}
