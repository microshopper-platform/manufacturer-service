package com.microshopper.manufacturerservice.service.impl;

import com.microshopper.authservice.web.api.AuthApi;
import com.microshopper.authservice.web.dto.AccountRegistration;
import com.microshopper.authservice.web.dto.AccountUpdate;
import com.microshopper.manufacturerservice.exception.AccountRegistrationFailedException;
import com.microshopper.manufacturerservice.exception.AccountUpdateFailedException;
import com.microshopper.manufacturerservice.mapper.ManufacturerAccountMapper;
import com.microshopper.manufacturerservice.model.ManufacturerAccount;
import com.microshopper.manufacturerservice.model.Role;
import com.microshopper.manufacturerservice.service.AuthIntegrationService;
import com.microshopper.manufacturerservice.web.dto.ManufacturerAccountDto;
import com.microshopper.manufacturerservice.web.dto.ManufacturerAccountRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AuthIntegrationServiceImpl implements AuthIntegrationService {

  private final AuthApi authApi;
  private final ManufacturerAccountMapper manufacturerAccountMapper;

  @Autowired
  public AuthIntegrationServiceImpl(
    AuthApi authApi,
    ManufacturerAccountMapper manufacturerAccountMapper) {

    this.authApi = authApi;
    this.manufacturerAccountMapper = manufacturerAccountMapper;
  }

  @Override
  public void registerAccountOnAuthService(Long id, ManufacturerAccountRegistrationDto manufacturerRegistrationDto, Role role) {

    AccountRegistration accountRegistration =
      manufacturerAccountMapper.mapToAccountRegistration(manufacturerRegistrationDto);
    accountRegistration.setRole(AccountRegistration.RoleEnum.fromValue(role.name()));
    accountRegistration.setId(id);

    ResponseEntity<Void> authAccountRegistrationResponse = authApi.registerAccountWithHttpInfo(accountRegistration);

    if (authAccountRegistrationResponse.getStatusCode().isError()) {
      throw new AccountRegistrationFailedException("Manufacturer creation failed. Account registration failed");
    }
  }

  @Override
  public void updateAccountOnAuthService(ManufacturerAccount manufacturerAccount) {

    AccountUpdate accountUpdate = manufacturerAccountMapper.mapToAccountUpdate(manufacturerAccount);

    ResponseEntity<Void> authUpdateResponse = authApi.updateAccountWithHttpInfo(accountUpdate);

    if (authUpdateResponse.getStatusCode().isError()) {
      throw new AccountUpdateFailedException("Manufacturer account update failed. Account update on auth service failed.");
    }
  }
}
