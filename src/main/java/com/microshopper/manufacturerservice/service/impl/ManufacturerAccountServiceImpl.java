package com.microshopper.manufacturerservice.service.impl;

import com.microshopper.manufacturerservice.exception.InvalidRegistrationCodeException;
import com.microshopper.manufacturerservice.exception.ManufacturerAccountNotFoundException;
import com.microshopper.manufacturerservice.mapper.ManufacturerAccountMapper;
import com.microshopper.manufacturerservice.mapper.ManufacturerMapper;
import com.microshopper.manufacturerservice.model.Manufacturer;
import com.microshopper.manufacturerservice.model.ManufacturerAccount;
import com.microshopper.manufacturerservice.model.RegistrationCode;
import com.microshopper.manufacturerservice.model.Role;
import com.microshopper.manufacturerservice.repository.ManufacturerAccountRepository;
import com.microshopper.manufacturerservice.repository.ManufacturerRepository;
import com.microshopper.manufacturerservice.repository.RegistrationCodeRepository;
import com.microshopper.manufacturerservice.service.AuthIntegrationService;
import com.microshopper.manufacturerservice.service.ManufacturerAccountService;
import com.microshopper.manufacturerservice.web.dto.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ManufacturerAccountServiceImpl implements ManufacturerAccountService {

  private final ManufacturerAccountRepository manufacturerAccountRepository;
  private final ManufacturerAccountMapper manufacturerAccountMapper;
  private final AuthIntegrationService authIntegrationService;
  private final ManufacturerMapper manufacturerMapper;
  private final ManufacturerRepository manufacturerRepository;
  private final RegistrationCodeRepository registrationCodeRepository;

  public ManufacturerAccountServiceImpl(
    ManufacturerAccountRepository manufacturerAccountRepository,
    ManufacturerAccountMapper manufacturerAccountMapper,
    AuthIntegrationService authIntegrationService,
    ManufacturerMapper manufacturerMapper,
    ManufacturerRepository manufacturerRepository,
    RegistrationCodeRepository registrationCodeRepository) {

    this.manufacturerAccountRepository = manufacturerAccountRepository;
    this.manufacturerAccountMapper = manufacturerAccountMapper;
    this.authIntegrationService = authIntegrationService;
    this.manufacturerMapper = manufacturerMapper;
    this.manufacturerRepository = manufacturerRepository;
    this.registrationCodeRepository = registrationCodeRepository;
  }

  @Override
  public ManufacturerAccountDto getManufacturerAccountByUsername(String username) {

    Optional<ManufacturerAccount> manufacturerAccountOptional =
      manufacturerAccountRepository.getManufacturerAccountByUsername(username);

    if (manufacturerAccountOptional.isEmpty()) {
      throw new ManufacturerAccountNotFoundException("Manufacturer account with username: " + username + " not found.");
    }

    ManufacturerAccount manufacturerAccount = manufacturerAccountOptional.get();
    Manufacturer manufacturer = manufacturerAccount.getManufacturer();

    ManufacturerAccountDto manufacturerAccountDto = manufacturerAccountMapper.mapToManufacturerAccountDto(manufacturerAccount);
    manufacturerAccountDto.setManufacturer(manufacturerMapper.mapToManufacturerDto(manufacturer));

    return manufacturerAccountDto;
  }

  @Override
  @Transactional
  public void registerManufacturer(ManufacturerRegistrationDto manufacturerRegistrationDto) {

    ManufacturerAccount manufacturerAccount =
      manufacturerAccountMapper
        .mapFromManufacturerAccountRegistrationDto(manufacturerRegistrationDto.getManufacturerAccountRegistration());
    manufacturerAccount.setRole(Role.ROLE_MANUFACTURER_ADMIN);

    Manufacturer manufacturer = manufacturerMapper
      .mapFromManufacturerDto(manufacturerRegistrationDto.getManufacturer());

    Manufacturer newManufacturer = manufacturerRepository.save(manufacturer);
    manufacturerAccount.setManufacturer(newManufacturer);
    ManufacturerAccount newManufacturerAccount = manufacturerAccountRepository.save(manufacturerAccount);

    authIntegrationService.registerAccountOnAuthService(
      newManufacturerAccount.getId(),
      manufacturerRegistrationDto.getManufacturerAccountRegistration(),
      Role.ROLE_MANUFACTURER_ADMIN);
  }

  @Override
  @Transactional
  public void registerManufacturerAccount(ManufacturerAccountRegistrationDto manufacturerAccountRegistrationDto) {

    ManufacturerAccount manufacturerAccount =
      manufacturerAccountMapper.mapFromManufacturerAccountRegistrationDto(manufacturerAccountRegistrationDto);

    Optional<RegistrationCode> registrationCodeOptional =
      registrationCodeRepository.findByCodeAndUsedIsFalse(manufacturerAccountRegistrationDto.getRegistrationCode());

    if (registrationCodeOptional.isEmpty()) {
      throw new InvalidRegistrationCodeException("Manufacturer account registration failed. Invalid registration code");
    }

    RegistrationCode registrationCode = registrationCodeOptional.get();

    manufacturerAccount.setRole(Role.valueOf(registrationCode.getRole().name()));
    manufacturerAccount.setManufacturer(registrationCode.getManufacturer());

    ManufacturerAccount newManufacturerAccount = manufacturerAccountRepository.save(manufacturerAccount);

    authIntegrationService.registerAccountOnAuthService(
      newManufacturerAccount.getId(),
      manufacturerAccountRegistrationDto,
      registrationCode.getRole());

    registrationCode.setUsed(true);

    registrationCodeRepository.save(registrationCode);
  }

  @Override
  public RegistrationCodeDto generateNewRegistrationCode(GenerateRegistrationCodeDto generateRegistrationCodeDto) {

    RoleDto roleDto = generateRegistrationCodeDto.getRole();

    RegistrationCode registrationCode = new RegistrationCode();

    Optional<Manufacturer> manufacturerOptional =
      manufacturerRepository.findById(generateRegistrationCodeDto.getManufacturerId());

    registrationCode.setCode(UUID.randomUUID().toString());
    registrationCode.setManufacturer(manufacturerOptional.get());
    registrationCode.setRole(Role.valueOf(roleDto.name()));

    RegistrationCode newRegistrationCode = registrationCodeRepository.save(registrationCode);

    return mapRegistrationCodeToDto(newRegistrationCode);
  }

  @Override
  public List<RegistrationCodeDto> getAllRegistrationCodes(Long manufacturerId) {

    return registrationCodeRepository.getAllByManufacturer_Id(manufacturerId)
      .stream()
      .map(this::mapRegistrationCodeToDto)
      .collect(Collectors.toList());
  }

  @Override
  public List<ManufacturerAccountDto> getAllManufacturerTransporters(Long manufacturerId) {

    Optional<Manufacturer> manufacturerOptional = manufacturerRepository.findById(manufacturerId);

    return manufacturerAccountRepository.getAllByManufacturerAndRole(manufacturerOptional.get(), Role.ROLE_MANUFACTURER_TRANSPORTER)
      .stream()
      .map(manufacturerAccountMapper::mapToManufacturerAccountDto)
      .collect(Collectors.toList());
  }

  @Override
  @Transactional
  public void updateManufacturerAccount(ManufacturerAccountDto manufacturerAccountDto) {

    String username = manufacturerAccountDto.getUsername();

    Optional<ManufacturerAccount> manufacturerAccountOptional =
      manufacturerAccountRepository.getManufacturerAccountByUsername(username);

    if (manufacturerAccountOptional.isEmpty()) {
      throw new ManufacturerAccountNotFoundException("Manufacturer account with username: " + username + " not found.");
    }

    ManufacturerAccount manufacturerAccount = manufacturerAccountOptional.get();

    manufacturerAccount.setEmail(manufacturerAccountDto.getEmail());

    ManufacturerAccount updatedManufacturerAccount = manufacturerAccountRepository.save(manufacturerAccount);

    authIntegrationService.updateAccountOnAuthService(manufacturerAccount);
  }

  private RegistrationCodeDto mapRegistrationCodeToDto(RegistrationCode registrationCode) {

    RegistrationCodeDto registrationCodeDto = new RegistrationCodeDto();
    registrationCodeDto.setCode(registrationCode.getCode());
    registrationCodeDto.setUsed(registrationCode.getUsed());
    registrationCodeDto.setRole(RoleDto.valueOf(registrationCode.getRole().name()));

    return registrationCodeDto;
  }
}
