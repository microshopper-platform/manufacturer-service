package com.microshopper.manufacturerservice.service.impl;

import com.microshopper.manufacturerservice.exception.ManufacturerNotFoundException;
import com.microshopper.manufacturerservice.mapper.ManufacturerMapper;
import com.microshopper.manufacturerservice.model.Manufacturer;
import com.microshopper.manufacturerservice.repository.ManufacturerRepository;
import com.microshopper.manufacturerservice.service.ManufacturerService;
import com.microshopper.manufacturerservice.web.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

  private final ManufacturerRepository manufacturerRepository;
  private final ManufacturerMapper manufacturerMapper;
  @Autowired
  public ManufacturerServiceImpl(
    ManufacturerRepository manufacturerRepository,
    ManufacturerMapper manufacturerMapper) {

    this.manufacturerRepository = manufacturerRepository;
    this.manufacturerMapper = manufacturerMapper;
  }

  @Override
  public List<ManufacturerDto> getAllManufacturers() {

    List<Manufacturer> manufacturers = manufacturerRepository.findAll();

    return manufacturers
      .stream()
      .map(manufacturerMapper::mapToManufacturerDto)
      .collect(Collectors.toList());
  }

  @Override
  public ManufacturerDto getManufacturerById(Long id) {

    Manufacturer manufacturer = manufacturerRepository.getOne(id);

    return manufacturerMapper.mapToManufacturerDto(manufacturer);
  }

  @Override
  public void addManufacturer(ManufacturerDto manufacturerDto) {

    Manufacturer manufacturer = manufacturerMapper.mapFromManufacturerDto(manufacturerDto);
    manufacturerRepository.save(manufacturer);
  }

  @Override
  public void updateManufacturer(ManufacturerDto manufacturerDto) {

    Long id = manufacturerDto.getId();
    Optional<Manufacturer> manufacturerOptional = manufacturerRepository.findById(id);

    if (manufacturerOptional.isEmpty()) {
      throw new ManufacturerNotFoundException("Manufacturer update failed. Manufacturer with id: " + id + " not found.");
    }

    Manufacturer manufacturer = manufacturerOptional.get();

    manufacturer.setDescription(manufacturerDto.getDescription());

    manufacturerRepository.save(manufacturer);
  }
}
