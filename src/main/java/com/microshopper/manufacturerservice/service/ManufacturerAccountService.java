package com.microshopper.manufacturerservice.service;

import com.microshopper.manufacturerservice.web.dto.*;

import java.util.List;

public interface ManufacturerAccountService {

  ManufacturerAccountDto getManufacturerAccountByUsername(String username);

  void registerManufacturer(ManufacturerRegistrationDto manufacturerRegistrationDto);

  void registerManufacturerAccount(ManufacturerAccountRegistrationDto manufacturerAccountRegistrationDto);

  RegistrationCodeDto generateNewRegistrationCode(GenerateRegistrationCodeDto generateRegistrationCodeDto);

  List<RegistrationCodeDto> getAllRegistrationCodes(Long manufacturerId);

  List<ManufacturerAccountDto> getAllManufacturerTransporters(Long manufacturerId);

  void updateManufacturerAccount(ManufacturerAccountDto manufacturerAccountDto);
}
