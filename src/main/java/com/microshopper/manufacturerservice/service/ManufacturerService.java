package com.microshopper.manufacturerservice.service;

import com.microshopper.manufacturerservice.web.dto.*;

import java.util.List;

public interface ManufacturerService {

  List<ManufacturerDto> getAllManufacturers();

  ManufacturerDto getManufacturerById(Long id);

  void addManufacturer(ManufacturerDto manufacturerDto);

  void updateManufacturer(ManufacturerDto manufacturerDto);
}
