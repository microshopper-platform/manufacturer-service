package com.microshopper.manufacturerservice.service;

import com.microshopper.manufacturerservice.model.ManufacturerAccount;
import com.microshopper.manufacturerservice.model.Role;
import com.microshopper.manufacturerservice.web.dto.ManufacturerAccountRegistrationDto;

public interface AuthIntegrationService {

  void registerAccountOnAuthService(
    Long id,
    ManufacturerAccountRegistrationDto manufacturerRegistrationDto,
    Role role);

  void updateAccountOnAuthService(ManufacturerAccount manufacturerAccount);
}
