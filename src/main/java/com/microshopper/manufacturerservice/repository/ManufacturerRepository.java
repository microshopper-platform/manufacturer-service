package com.microshopper.manufacturerservice.repository;

import com.microshopper.manufacturerservice.model.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
}
