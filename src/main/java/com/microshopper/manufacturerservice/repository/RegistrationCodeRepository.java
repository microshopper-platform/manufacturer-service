package com.microshopper.manufacturerservice.repository;

import com.microshopper.manufacturerservice.model.RegistrationCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RegistrationCodeRepository extends JpaRepository<RegistrationCode, Long> {

  Optional<RegistrationCode> findByCodeAndUsedIsFalse(String code);

  List<RegistrationCode> getAllByManufacturer_Id(Long manufacturerId);
}
