package com.microshopper.manufacturerservice.repository;

import com.microshopper.manufacturerservice.model.Manufacturer;
import com.microshopper.manufacturerservice.model.ManufacturerAccount;
import com.microshopper.manufacturerservice.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ManufacturerAccountRepository extends JpaRepository<ManufacturerAccount, Long> {

  Optional<ManufacturerAccount> getManufacturerAccountByUsername(String username);

  List<ManufacturerAccount> getAllByManufacturerAndRole(Manufacturer manufacturer, Role role);
}
