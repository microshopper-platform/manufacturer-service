package com.microshopper.manufacturerservice.exception;

public class AccountRegistrationFailedException extends RuntimeException {

  public AccountRegistrationFailedException(String message) {
    super(message);
  }
}
