package com.microshopper.manufacturerservice.exception;

public class ManufacturerNotFoundException extends RuntimeException {

  public ManufacturerNotFoundException(String message) {
    super(message);
  }
}
