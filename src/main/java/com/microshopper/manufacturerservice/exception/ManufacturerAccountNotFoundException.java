package com.microshopper.manufacturerservice.exception;

public class ManufacturerAccountNotFoundException extends RuntimeException {

  public ManufacturerAccountNotFoundException(String message) {
    super(message);
  }
}
