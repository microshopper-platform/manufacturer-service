package com.microshopper.manufacturerservice.exception;

public class InvalidRegistrationCodeException extends RuntimeException {

  public InvalidRegistrationCodeException(String message) {
    super(message);
  }
}
