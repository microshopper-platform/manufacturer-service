package com.microshopper.manufacturerservice.exception;

public class AccountUpdateFailedException extends RuntimeException {

  public AccountUpdateFailedException(String message) {
    super(message);
  }
}
