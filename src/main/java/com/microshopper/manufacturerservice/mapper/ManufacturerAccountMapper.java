package com.microshopper.manufacturerservice.mapper;

import com.microshopper.authservice.web.dto.AccountRegistration;
import com.microshopper.authservice.web.dto.AccountUpdate;
import com.microshopper.manufacturerservice.model.ManufacturerAccount;
import com.microshopper.manufacturerservice.web.dto.ManufacturerAccountDto;
import com.microshopper.manufacturerservice.web.dto.ManufacturerAccountRegistrationDto;
import org.springframework.stereotype.Component;

@Component
public class ManufacturerAccountMapper {

  public AccountRegistration mapToAccountRegistration(
    ManufacturerAccountRegistrationDto manufacturerAccountRegistrationDto) {

    AccountRegistration accountRegistration = new AccountRegistration();
    accountRegistration.setUsername(manufacturerAccountRegistrationDto.getUsername());
    accountRegistration.setEmail(manufacturerAccountRegistrationDto.getEmail());
    accountRegistration.setPassword(manufacturerAccountRegistrationDto.getPassword());

    return accountRegistration;
  }

  public AccountUpdate mapToAccountUpdate(ManufacturerAccount manufacturerAccount) {

    AccountUpdate accountUpdate = new AccountUpdate();
    accountUpdate.setUsername(manufacturerAccount.getUsername());
    accountUpdate.setEmail(manufacturerAccount.getEmail());
    accountUpdate.setRole(AccountUpdate.RoleEnum.fromValue(manufacturerAccount.getRole().name()));

    return accountUpdate;
  }

  public ManufacturerAccountDto mapToManufacturerAccountDto(
    ManufacturerAccount manufacturerAccount) {

    ManufacturerAccountDto manufacturerAccountDto = new ManufacturerAccountDto();
    manufacturerAccountDto.setId(manufacturerAccount.getId());
    manufacturerAccountDto.setUsername(manufacturerAccount.getUsername());
    manufacturerAccountDto.setEmail(manufacturerAccount.getEmail());

    return manufacturerAccountDto;
  }

  public ManufacturerAccount mapFromManufacturerAccountRegistrationDto(
    ManufacturerAccountRegistrationDto manufacturerAccountRegistrationDto) {

    ManufacturerAccount manufacturerAccount = new ManufacturerAccount();
    manufacturerAccount.setUsername(manufacturerAccountRegistrationDto.getUsername());
    manufacturerAccount.setEmail(manufacturerAccountRegistrationDto.getEmail());

    return manufacturerAccount;
  }
}
