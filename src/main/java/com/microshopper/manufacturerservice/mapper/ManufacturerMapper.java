package com.microshopper.manufacturerservice.mapper;

import com.microshopper.manufacturerservice.model.Manufacturer;
import com.microshopper.manufacturerservice.web.dto.*;
import org.springframework.stereotype.Component;

@Component
public class ManufacturerMapper {

  public ManufacturerDto mapToManufacturerDto(Manufacturer manufacturer) {

    ManufacturerDto manufacturerDto = new ManufacturerDto();
    manufacturerDto.setId(manufacturer.getId());
    manufacturerDto.setName(manufacturer.getName());
    manufacturerDto.setDescription(manufacturer.getDescription());

    return manufacturerDto;
  }

  public Manufacturer mapFromManufacturerDto(ManufacturerDto manufacturerDto) {

    Manufacturer manufacturer = new Manufacturer();
    manufacturer.setName(manufacturerDto.getName());
    manufacturer.setDescription(manufacturerDto.getDescription());

    return manufacturer;
  }
}
